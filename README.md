# Jitsi-Desktop [jitsi-meet-electron](https://github.com/jitsi/jitsi-meet-electron) with Docker

### Usage
```
git clone https://git.manalejandro.com/ale/jitsi-desktop
xhost +local:$(hostname)
cd jitsi-desktop
docker-compose up -d
```

### License
```
MIT
```
