FROM node:14-buster-slim
RUN mkdir -p /usr/share/man/man1 /usr/share/man/man5 /usr/share/man/man7
RUN apt update && apt -y upgrade && apt -y install python xorg-dev libxshmfence1 \
                       build-essential clang libdbus-1-dev libgtk-3-dev \
                       libnotify-dev alsa-utils alsa-tools libasound2-plugins \
                       libasound2-dev libcap-dev libcups2-dev libxtst-dev \
                       libxss1 libnss3-dev gcc-multilib g++-multilib curl git \
                       gperf bison python-dbusmock default-jre xvfb && apt clean
RUN git clone https://github.com/jitsi/jitsi-meet-electron /jitsi-meet-electron
RUN adduser node audio && adduser node video && chown node.node -R /jitsi-meet-electron
WORKDIR /jitsi-meet-electron
USER node
RUN yarn
USER root
RUN chown root.root /jitsi-meet-electron/node_modules/electron/dist/chrome-sandbox
RUN chmod 4755 /jitsi-meet-electron/node_modules/electron/dist/chrome-sandbox
USER node
RUN yarn add jsonfile cli-truncate
